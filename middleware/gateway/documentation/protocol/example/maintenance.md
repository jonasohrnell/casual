# interdomain protocol examples

This directory contains binary dumps of examples of every interdomain message that casual uses.

Main objective is to help other implementations to verify and unittest.

## naming convention

`<message-name>.<message-type>.bin`


