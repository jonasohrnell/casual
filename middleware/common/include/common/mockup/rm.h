//! 
//! Copyright (c) 2015, The casual project
//!
//! This software is licensed under the MIT license, https://opensource.org/licenses/MIT
//!

#pragma once


extern "C"
{
   extern struct xa_switch_t casual_mockup_xa_switch_static;
}

