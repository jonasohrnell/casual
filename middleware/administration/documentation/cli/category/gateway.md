# gateway `casual CLI`

``` 
  gateway  0..1
        gateway related administration

    SUB OPTIONS
      -c, --list-connections  0..1
            list all connections

      -l, --list-listeners  0..1
            list all listeners

      --state  0..1  ([json, yaml, xml, ini]) [0..1]
            gateway state

```